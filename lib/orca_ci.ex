defmodule OrcaCI do
  @moduledoc """
  Documentation for OrcaCI.
  """

  @doc """
  Hello world.

  ## Examples

      iex> OrcaCI.build_directory
      "./builds_dir/"

  """
  def build_directory do
    "./builds_dir/"
  end
end
