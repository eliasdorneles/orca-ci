defmodule OrcaCI.BuildWorker do
  def run_build_for(repo_url, branch_name, commit) do
    git_workflow(repo_url, branch_name, commit)
    run_build(work_dir(repo_url, branch_name))
  end

  def work_dir(repo_url, branch_name) do
    Path.join([OrcaCI.build_directory(), Path.basename(repo_url), branch_name])
  end

  def git_workflow(repo_url, branch_name, _commit) do
    repo = Git.clone!([repo_url, work_dir(repo_url, branch_name)])
    Git.checkout!(repo, branch_name)
  end

  def run_build(work_directory) do
    config_path = Path.join([work_directory, ".circleci", "config.yml"])
    if File.exists?(config_path) do
      [config] = YamlElixir.read_all_from_file(config_path)
      run_build_steps(config, work_directory)
    end
  end

  def run_build_steps(config, work_directory) do
    steps = get_in(config, ["jobs", "build", "steps"])
    for step <- steps do
      if step != "checkout" do
        run_cmd(step["run"], work_directory)
      end
    end
  end

  def run_cmd(cmd, cwd) do
        :erlsh.oneliner(String.to_charlist(cmd), String.to_charlist(cwd))
  end
end
