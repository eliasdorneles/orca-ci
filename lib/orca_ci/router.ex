defmodule OrcaCI.Router do
  use Plug.Router

  plug :match
  plug Plug.Parsers, parsers: [:urlencoded, :multipart, :json],
                     pass: ["application/json"],
                     json_decoder: Jason
  plug :dispatch

  get "/hello" do
    send_resp(conn, 200, "world")
  end

  post "/webhook" do
    IO.inspect(conn.body_params)
    push_event = conn.body_params
    url = push_event["repository"]["git_http_url"]
    [{:done, 0, result}] = OrcaCI.BuildWorker.run_build_for(url, "master", nil)
    send_resp(conn, 200, Jason.encode!(result))
  end

  # forward "/users", to: UsersRouter

  match _ do
    send_resp(conn, 404, "oops")
  end
end
