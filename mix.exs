defmodule OrcaCI.MixProject do
  use Mix.Project

  def project do
    [
      app: :orca_ci,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :cowboy, :plug],
      mod: {OrcaCI.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:yaml_elixir, "~> 1.3"},
      {:git_cli, "~> 0.2.4"},
      {:erlsh, "~> 0.1.0"},
      {:plug, "~> 1.5"},
      {:cowboy, "~> 1.1"},
      {:jason, "~> 1.0"}
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
    ]
  end
end
